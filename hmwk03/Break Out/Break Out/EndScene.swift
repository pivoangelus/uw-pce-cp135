//
//  EndScene.swift
//  Break Out
//
//  Created by Justin Andros on 11/9/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SpriteKit

class EndScene: SKScene {
    let gameOverSound = SKAction.playSoundFileNamed("Sounds/gameover.caf", waitForCompletion: false)
    
    init(size: CGSize, win: Bool, finalScore: Int) {
        super.init(size: size)
        backgroundColor = SKColor.blackColor()

        let endLabel = SKLabelNode(fontNamed: "Future Medium")
        endLabel.fontColor = SKColor.whiteColor()
        endLabel.fontSize = 50
        endLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        if win {
            endLabel.text = "YOU WIN!"
        } else {
            endLabel.text = "GAME OVER"
            runAction(gameOverSound)
        }
        addChild(endLabel)
        
        let scoreLabel = SKLabelNode(fontNamed: "Arial")
        scoreLabel.fontColor = SKColor.whiteColor()
        scoreLabel.fontSize = 24
        scoreLabel.text = "Score: \(finalScore)"
        scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) - 30)
        addChild(scoreLabel)
        
        let tryAgainLabel = SKLabelNode(fontNamed: "Futura Medium")
        tryAgainLabel.fontColor = SKColor.whiteColor()
        tryAgainLabel.fontSize = 24
        tryAgainLabel.text = "tap to play again"
        tryAgainLabel.position = CGPointMake(CGRectGetMidX(self.frame), -70)
        let moveLabel = SKAction.moveToY(size.height / 2 - 60, duration: 2.0)
        tryAgainLabel.runAction(moveLabel)
        addChild(tryAgainLabel)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let scene = GameScene(size: self.size)
        let reveal = SKTransition.doorsOpenHorizontalWithDuration(2.0)
        self.view?.presentScene(scene, transition: reveal)
    }
}