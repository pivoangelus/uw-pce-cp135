//
//  GameScene.swift
//  Break Out
//
//  Created by Justin Andros on 11/9/15.
//  Copyright (c) 2015 Justin Andros. All rights reserved.
//

import SpriteKit

struct BreakOutCategory {
    static let noneCategory: UInt32         = 0x1
    static let ballCategory: UInt32         = 0x1 << 1
    static let brickCategory: UInt32        = 0x1 << 2
    static let paddleCategory: UInt32       = 0x1 << 3
    static let edgeCategory: UInt32         = 0x1 << 4
    static let bottomEdgeCategory: UInt32   = 0x1 << 5
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    var bricks = 0
    let paddle = SKSpriteNode(imageNamed: "paddle")
    let scoreboard = SKLabelNode(fontNamed: "Arial")
    let paddleSound = SKAction.playSoundFileNamed("Sounds/blip.caf", waitForCompletion: false)
    let brickSound = SKAction.playSoundFileNamed("Sounds/brickhit.caf", waitForCompletion: false)
    
    override func didMoveToView(view: SKView) {
        backgroundColor = SKColor.blackColor()
        physicsWorld.gravity = CGVectorMake(0, 0)
        physicsWorld.contactDelegate = self
        physicsBody = SKPhysicsBody(edgeLoopFromRect: frame)
        physicsBody?.categoryBitMask = BreakOutCategory.edgeCategory
        
        addBall()
        addPlayer()
        addBricks()
        addBottomEdge()
        addScore()
    }
    
    override func update(currentTime: NSTimeInterval) {
        if bricks == 4 {
            removeAllChildren()
            let endScene = EndScene(size: self.size, win: true, finalScore: bricks * 100)
            endScene.scaleMode = .ResizeFill
            let reveal = SKTransition.doorsCloseHorizontalWithDuration(1.0)
            view?.presentScene(endScene, transition: reveal)
        }
    }
    
    // MARK: elements added to the scene
    
    func addBall() {
        let ball = SKSpriteNode(imageNamed: "ball")
        ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.frame.size.width / 2)
        ball.physicsBody?.friction = 0.0
        ball.physicsBody?.linearDamping = 0.0
        ball.physicsBody?.restitution = 1.0
        ball.physicsBody?.categoryBitMask = BreakOutCategory.ballCategory
        ball.physicsBody?.contactTestBitMask = BreakOutCategory.brickCategory | BreakOutCategory.paddleCategory | BreakOutCategory.bottomEdgeCategory
        ball.position = CGPoint(x: size.width / 2, y: size.height / 2)
        addChild(ball)
        
        let impulse = CGVectorMake(10, 10)
        ball.physicsBody?.applyImpulse(impulse)
    }
    
    func addPlayer() {
        paddle.position = CGPointMake(size.width / 2, 100)
        paddle.physicsBody = SKPhysicsBody(rectangleOfSize: paddle.frame.size)
        paddle.physicsBody?.dynamic = false
        paddle.physicsBody?.categoryBitMask = BreakOutCategory.paddleCategory
        addChild(paddle)
    }
    
    func addBricks() {
        // to keep the game super quick only 4 bricks :(
        for i in 0...3 {
            let brick = SKSpriteNode(imageNamed: "brick")
            brick.physicsBody = SKPhysicsBody(rectangleOfSize: brick.frame.size)
            brick.physicsBody?.dynamic = false
            brick.physicsBody?.categoryBitMask = BreakOutCategory.brickCategory
            let xPos = size.width / 5 * (CGFloat(i) + 1)
            let yPos = size.height - 50
            brick.position = CGPointMake(xPos, yPos)
            addChild(brick)
        }
    }
    
    func addBottomEdge() {
        let bottomEdge = SKNode()
        bottomEdge.physicsBody = SKPhysicsBody(edgeFromPoint: CGPoint(x: 0, y: 1), toPoint: CGPoint(x: size.width, y: 1))
        bottomEdge.physicsBody?.categoryBitMask = BreakOutCategory.bottomEdgeCategory
        addChild(bottomEdge)
    }
    
    func addScore() {
        scoreboard.fontColor = SKColor.whiteColor()
        scoreboard.fontSize = 20
        scoreboard.position = CGPointMake(size.width / 6, 25)
        scoreboard.text = "Score: 0"
        addChild(scoreboard)
    }
    
    // MARK: game updates
    
    func updateScore() {
        let tempScore = bricks * 100
        scoreboard.text = "Score: \(tempScore)"
    }
    
    // MARK: touches
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            var newPosition = CGPointMake(location.x, 100)
            if newPosition.x < paddle.size.width / 2 {
                newPosition.x = paddle.size.width / 2
            }
            if newPosition.x > size.width - (paddle.size.width / 2) {
                newPosition.x = size.width - (paddle.size.width / 2)
            }
            paddle.position = newPosition
        }
    }
    
    // MARK: SKPhysicsContactDelegate
    
    func didBeginContact(contact: SKPhysicsContact) {
        // nonball object
        let body: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            body = contact.bodyB
        } else {
            body = contact.bodyA
        }
        
        if body.categoryBitMask == BreakOutCategory.brickCategory {
            runAction(brickSound)
            bricks++
            updateScore()
            let wait = SKAction.waitForDuration(1.5)
            let brickDeathPath = NSBundle.mainBundle().pathForResource("BrickDeath", ofType: "sks")!
            let brickDeathEmitter = NSKeyedUnarchiver.unarchiveObjectWithFile(brickDeathPath) as! SKEmitterNode
            brickDeathEmitter.zPosition = 99
            body.node?.addChild(brickDeathEmitter)
            body.node?.runAction(wait, completion: { _ in
                body.node?.removeFromParent()
            })
        }
        
        if body.categoryBitMask == BreakOutCategory.paddleCategory {
            runAction(paddleSound)
        }
        
        if body.categoryBitMask == BreakOutCategory.bottomEdgeCategory {
            removeAllChildren()
            let endScene = EndScene(size: self.size, win: false, finalScore: bricks * 100)
            endScene.scaleMode = .ResizeFill
            let reveal = SKTransition.doorsCloseHorizontalWithDuration(1.0)
            view?.presentScene(endScene, transition: reveal)
        }
    }
}
