//
//  MainMenuScene.swift
//  Break Out
//
//  Created by Justin Andros on 11/9/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SpriteKit

class MainMenuScene: SKScene {
    override func didMoveToView(view: SKView) {
        backgroundColor = SKColor.blackColor()
        
        let mainMenuLabel = SKLabelNode(fontNamed: "Future Medium")
        mainMenuLabel.fontColor = SKColor.whiteColor()
        mainMenuLabel.fontSize = 50
        mainMenuLabel.text = "BREAK OUT"
        mainMenuLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        self.addChild(mainMenuLabel)
        
        let startLabel = SKLabelNode(fontNamed: "Futura Medium")
        startLabel.fontColor = SKColor.whiteColor()
        startLabel.fontSize = 24
        startLabel.text = "tap to play"
        startLabel.position = CGPointMake(CGRectGetMidX(self.frame), -50)
        let moveLabel = SKAction.moveToY(size.height / 2 - 40, duration: 2.0)
        startLabel.runAction(moveLabel)
        self.addChild(startLabel)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let scene = GameScene(size: self.size)
        let reveal = SKTransition.doorsOpenHorizontalWithDuration(2.0)
        self.view?.presentScene(scene, transition: reveal)
    }
}