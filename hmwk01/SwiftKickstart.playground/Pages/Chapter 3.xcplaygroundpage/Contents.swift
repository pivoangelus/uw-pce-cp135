//: ## Chapter 3 - Variables and Constants
//: [TOC](TOC) | [Next](@next)
//: ### let, var, and Types
let person: String = "Swift Programmer"
//person = "some new value" -- let marks a variable as immutable
var greeting = "Hello, "
greeting = "Hi there, "
// greeting = 7 -- Cannot assign a value of type 'Int' to a value of type 'String'
greeting + person

//func hello(peopleNamed people: String ...) {
//    for person in people {
//        print("Hello, \(person)")
//    }
//}

let shouldBeRed: Bool
shouldBeRed = true

let stringWithoutValue: String

if shouldBeRed {
    stringWithoutValue = "red"
} else {
    stringWithoutValue = "blue"
}

func hellos(name: String, numberOfTimes: Int) -> String {
    var tempGreeting = ""
    for _ in 1 ... numberOfTimes {
        tempGreeting += "Hello, \(name)!\n"
    }
    return tempGreeting
}

greeting = hellos(person, numberOfTimes: 3)
let greetingFunction = hellos
greeting = greetingFunction("Surprised Reader", numberOfTimes: 4)

//: ### Numberic Types
let simpleProduct = 5 * 3.4
let five = 5
let threePointFour = 3.4
//let product = five * threePointFour -- no automatic promotion of type
let fiveAsDouble = Double(five)
let product = fiveAsDouble * threePointFour

//: ### nil
var name: String?
name = "Daniel"
print(name!)
name = nil
//print(name!) -- error

import AppKit

let image = NSImage(named: "Kickstart.jpg")
let imageView = NSImageView()
if let validImage = image {
    imageView.image = validImage
} else {
    print("No image found.")
}
imageView.image

//: ### Higher Order Functions
let friend = "Joe"
func hello(name: String) -> String {
    let greeting = "Hello, \(name)!"
    return greeting
}
hello(friend)

func bonjour(name: String) -> String {
    let greeting = "Bonjour, \(name)!"
    return greeting
}
bonjour(friend)

func greet(name: String, withSalutation salutation: String) -> String {
    let greeting = "\(salutation), \(name)!"
    return greeting
}
greet(friend, withSalutation: "Hi there")

func createGreetingWithSalutation(salutation: String) -> (String) -> String {
    func greetingFunction(name: String) -> String {
        let greeting = "\(salutation), \(name)!"
        return greeting
    }
    return greetingFunction
}
let heyThere = createGreetingWithSalutation("Hey there")
let yo = createGreetingWithSalutation
heyThere(friend)
createGreetingWithSalutation("Hi")(friend)
yo("Yo")(friend)

func useGreeting(greetingFor: (String) -> String, forPeopleWithName people: String ...) -> String {
    var tempGreeting = ""
    for person in people {
        tempGreeting += greetingFor(person) + "\n"
    }
    return tempGreeting
}
print(useGreeting(heyThere, forPeopleWithName: "Swift Programmer", "Swift 2 Adopter"))

//: ### typealias
/// Used to measure payments in US Dollars
typealias USDollars = Double
typealias Count = Int
typealias RevenueCalculator = (Count) -> USDollars

/** The return value is the revenue in US Dollars */
func revenueForCopies(numberSold: Count) -> USDollars {
    return Double(numberSold * 99 * 70/100)/100
}
revenueForCopies(6)

func totalRevenue(calculator: RevenueCalculator, copies: Int ...) -> Double {
    var runningTotal = 0.00
    for copy in copies {
        runningTotal += calculator(copy)
    }
    return runningTotal
}
totalRevenue(revenueForCopies, copies: 6, 3, 1)
