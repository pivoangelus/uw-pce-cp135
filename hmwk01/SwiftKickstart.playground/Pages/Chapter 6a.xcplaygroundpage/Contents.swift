//: ## Chapter 6a - Enumerations
//: [TOC](TOC)
import AppKit

extension NSView {
    var backgroundColor: NSColor? {
        get {
            if let colorRef = self.layer?.backgroundColor {
                return NSColor(CGColor: colorRef)
            } else {
                return nil
            }
        }
        set {
            self.wantsLayer = true
            self.layer?.backgroundColor = newValue?.CGColor
        }
    }
}

enum Color {
    case Red
    case Green
    case Blue
    
    //fallthrough -- used to continue through the cases. default is now to exit out once a match has been done
    func actualColor() -> NSColor {
        switch self {
        case .Red:
            return NSColor.redColor()
        case .Green:
            return NSColor.greenColor()
        case .Blue:
            return NSColor.blueColor()
        }
    }

    func colorSwatch(width width: Int, height: Int) -> NSView {
        let myView = NSView(frame: CGRect(x: 0, y:0, width: width, height: height))
        myView.backgroundColor = self.actualColor()
        return myView
    }
}


var crayon = Color.Green
crayon = .Blue
let paintBrushColor: Color
paintBrushColor = .Red

var crayonSwitch = Color.Green
crayonSwitch.actualColor()
crayonSwitch = .Red
crayonSwitch.actualColor()
crayonSwitch.colorSwatch(width: 200, height: 100)
