//: ## Chapter 6c - Enumerations
//: [TOC](TOC)
import AppKit

enum Color: String {
    case Red = "Maraschinto"
    case Green
    case Blue = "Blueberry"
}

var crayon = Color.Blue
crayon.rawValue
crayon = .Green
crayon.rawValue

enum ColorCode: CGFloat {
    case Yellow = 0.1667
    case Green  = 0.3333
    case Blue   = 0.6667
    case Purple = 0.8333

    var actualColor: NSColor {
        return NSColor(hue: rawValue, saturation: 1, brightness: 1, alpha: 1)
    }
}

var crayonCode = ColorCode.Blue
crayonCode.actualColor

//: ### associated values
enum PrimaryColor: String {
    case Red
    case Yellow
    case Blue
}


enum Desktop {
    case Black
    case White
    case Color(PrimaryColor)
    case Tiled(PrimaryColor, PrimaryColor, Int, Int)
}

//let blackBackground = Desktop.Black
//let whiteBackground = Desktop.White
//let redBackground = Desktop.Color(PrimaryColor.Red)
//let yellowBackground = Desktop.Color(.Yellow)

let backgrounds = [Desktop.Black, Desktop.Color(.Red), Desktop.White, Desktop.Color(.Blue)]

for background in backgrounds {
    switch background {
    case .Color(let primaryColor):
        print("This background color is \(primaryColor.rawValue)")
    default:
        print("No background color")
    }
}

for background in backgrounds {
    if case .Color(let primaryColor) = background where primaryColor == .Red {
        print("Red!")
    } else {
        print("for if case: Not red")
    }
}

for case .Color(let primaryColor) in backgrounds where primaryColor != .Red {
    print("for case: not red")
}

//: ### errors
enum InputSizeError: ErrorType {
    case NegativeNumberError
    case NumberIsTooLargeError(amountOver: Int)
}

func double(input: Int) throws -> Int {
    if input < 0 {
        throw InputSizeError.NegativeNumberError
    } else if input > 49 {
        throw InputSizeError.NumberIsTooLargeError(amountOver: input - 49)
    } else {
        return input * 2
    }
}

func cautiousDoubler(input: Int) {
    do {
        print("double", input, "=", try double(input))
    }
    catch InputSizeError.NumberIsTooLargeError(let excess) {
        print("Sorry, input is too big by", excess)
    }
    catch {
        print("Error:", error)
    }
}

cautiousDoubler(8)
cautiousDoubler(-8)
cautiousDoubler(80)
let six = try? double(3)
let negTwo = try? double(-1)
let twoHundred = try? double(100)

let ints = [6, -1, 12, 100, 20]
let doubleInts = ints.flatMap{element in try? double(element)}
doubleInts

//: ### option sets
struct ColorsWeLove: OptionSetType {
    let rawValue: Int
    static let Red = ColorsWeLove(rawValue: 1)
    static let Green = ColorsWeLove(rawValue: 2)
    static let Blue = ColorsWeLove(rawValue: 4)
}

var noColors: ColorsWeLove = []
noColors.contains(.Red)
var twoColors: ColorsWeLove = [.Red, .Blue]
twoColors.contains(.Red)
twoColors.contains(.Green)