//: ## Chapter 5 - Flexible Functions
//: [TOC](TOC)
//: ### Function Parameters
var fp_two = 2
func fp_double(var input: Int) {
    input
    input = input * 2
}
fp_double(fp_two)
fp_two

//: ### Reference Types
import AppKit

var textField = NSTextField()
textField.stringValue = "Hello"

func modifyField(field: NSTextField) {
    field.stringValue = "Goodbye"
    field.stringValue
}
modifyField(textField)
textField.stringValue

//: ### inout parameters
var inout_two = 2
func inoutDouble(inout input: Int) {
    input = input * 2
}
inout_two
inoutDouble(&inout_two)
inoutDouble(&inout_two)
inout_two

//: ### Return Values
//set1.unionInPlace(set2) -- set1 is replaced by set2
//set1 = set1.union(set2) -- set1 becomes the union of set1 and set2
var rv_two = 2
func rv_double(input: Int) -> Int {
    return input * 2 // * = func *(lhs: Int, rhs: Int) -> Int
}
rv_double(rv_two)
rv_two
rv_two = rv_double(rv_two)
rv_two

//: ### Extensions
extension Int {
    mutating func doubleInPlace() {
        self = self * 2
    }
    func double() -> Int {
        return self * 2
    }
}
var e_two = 2
//two.doubleInPlace()
e_two
e_two.double()
e_two
e_two = e_two.double()
e_two

//: ### Higher Order Functions
let two = 2
func hof_double(input: Int) -> Int {
    return input * 2
}
let hof_four = hof_double(two)

func hof_modify(input: Int, byApplying f: (Int) -> Int) -> Int {
    return f(input)
}
let hof_eight = hof_modify(hof_four, byApplying: hof_double)

func hof_addSeven(input: Int) -> Int {
    //let result = input + 7
    //return result
    return input + 7
}
let hof_fifteen = hof_modify(hof_eight, byApplying: hof_addSeven)

//: ### Closure Expressions
func ce_modify(input: Int, byApplying f: (Int) -> Int) -> Int {
    return f(input)
}
let ce_four = ce_modify(2, byApplying: {(input: Int) -> Int in
                                    return input * 2
                                 })
let ce_six = ce_modify(3){(input: Int) -> Int in return input * 2}
let ce_eight = ce_modify(4){input in return input * 2}
let ce_ten = ce_modify(5){input in input * 2}
let ce_twelve = ce_modify(6){$0 * 2}

//: ### Generics
/// Int
func g_modify(input: Int, byApplying f: (Int) -> Int) -> Int {
    return f(input)
}
/// Double
func g_modify(input: Double, byApplying f: (Double) -> Double) -> Double {
    return f(input)
}
let g_two = g_modify(1){input in input * 2}
let g_three = g_modify(1.5){input in input * 2.0}

func g_modify<T>(input: T, byApplying f: (T) -> T) -> T {
    return f(input)
}
let g_four = g_modify(2){input in input * 2}
let g_five = g_modify(2.5){input in input * 2.0}
let greeting = g_modify("Hello"){input in input + ", World!"}

//: ### Transforming Arrays
func ta_transform<T, U>(input: [T], byApplying f: (T) -> U) -> [U] {
    var transformedArray = [U]()
    for element in input {
        transformedArray.append(f(element))
    }
    return transformedArray
}
let numberOfCopies = [9, 12, 7, 15, 20]
let isBiggerThanTen = ta_transform(numberOfCopies){$0 > 10}
let revenue = ta_transform(numberOfCopies){Double($0) * 0.99 * 0.70}

//: ### Understanding map()
extension Array {
    func transform<U>(f:(Element) -> U) -> [U] {
        var transformedArray = [U]()
        for element in self {
            transformedArray.append(f(element))
        }
        return transformedArray
    }
}
let revenue_2 = numberOfCopies.transform{Double($0) * 0.99 * 0.70}
let isBiggerThanTenMap = numberOfCopies.map{$0 > 10}
let revenueMap = numberOfCopies.map{Double($0) * 0.99 * 0.70}
