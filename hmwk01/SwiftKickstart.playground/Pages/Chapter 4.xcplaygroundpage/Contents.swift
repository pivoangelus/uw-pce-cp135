//: ## Chapter 4 - Collections
//: [TOC](TOC) | [Next](@next)
//: ### One Type
let numbers = [0, 1, 2] // [Int]
let three = numbers[1] + numbers[2]

import Foundation
let badNumbers = [0, 1, 2, "Hello"] // errors without import Foundation. [NSObject] now
//let badThree = badNumbers[1] + badNumbers[2]
let badThree = (badNumbers[0] as! NSNumber).integerValue + (badNumbers[1] as! NSNumber).integerValue

//: ### Array Creation
let people = ["Swift Programmer", "Obj-C Old-timer"]
let morePeople: [String]
// sometime later...
morePeople = ["Swift 2 Adopter", "Protocol Pro"]

func createGreetingWithSalutation(salutation: String) -> (String) -> String {
    func greetingFunction(name: String) -> String {
        let greeting = "\(salutation), \(name)!"
        return greeting
    }
    return greetingFunction
}
let greetings = [createGreetingWithSalutation("Hello"),
                 createGreetingWithSalutation("Bonjour"),
                 createGreetingWithSalutation("Hey")]
let helloSwiftProgrammer = greetings[0](people[0])

let salutations = ["Hello", "Bonjour", "Hey"]
let simplerGreetings = salutations.map(createGreetingWithSalutation)
let heyProtocolPro = simplerGreetings[2](morePeople[1])

//var coffeeDrinks = [String]()
//coffeeDrinks.count

//: ### Array Mutation
var drinkSizes = Array(count: 3, repeatedValue: "Really Big")
drinkSizes[1] = "Big"
drinkSizes.append("Huge")
drinkSizes.insert("Moderate", atIndex: 1)
drinkSizes[0 ... 1] = ["Medium"]
drinkSizes[2 ... 2] = ["Bigger", "Jumbo"]
drinkSizes += ["Huger"]
drinkSizes = drinkSizes + ["Hugest"]
//drinkSizes[25] = "Out of Range"
drinkSizes.removeLast()
drinkSizes.removeAtIndex(2)
drinkSizes[2 ... 3] = []
drinkSizes.removeAll(keepCapacity: true)
if !drinkSizes.isEmpty {
    drinkSizes.removeLast()
}
drinkSizes.count
drinkSizes

//: ### Enumeration
let coffeDrinks = ["Drip", "Espresso", "Americano", "Cappuccino"]
for i in 0 ..< coffeDrinks.count {
    print(coffeDrinks[i])
}
for kindOfDrink in coffeDrinks {
    print(kindOfDrink)
}
for (index, kindOfDrink) in coffeDrinks.enumerate() {
    print("[\(index)] \(kindOfDrink)")
}

//: ### Value and Reference Types
var x = 7
var y = x
x = 5
print("x = \(x), y = \(y)")

import AppKit

var xField = NSTextField()
xField.stringValue = "x"
var yField = xField
print("x = \(xField.stringValue), y = \(yField.stringValue)")
yField.stringValue = "y"
print("x = \(xField.stringValue), y = \(yField.stringValue)")

var xNumbers = [0, 1, 2]
var yNumbers = xNumbers
xNumbers[0] = 100
print("x = \(xNumbers), y = \(yNumbers)")

var xFields = [xField, yField]
var yFields = xFields

var zField = NSTextField()
zField.stringValue = "z"
yFields[0] = zField
print("x = \(xFields[0].stringValue), y = \(yFields[0].stringValue)")
yFields[1].stringValue = "Changed"
print("x = \(xFields[1].stringValue), y = \(yFields[1].stringValue)")

//: ### Dictionaries
let dictionary = ["one": 1, "two": 2, "three": 3]
let moreDictionary: [String: Int] // [KeyType: ValueType]
let dictionaryKeys = dictionary.keys
let dictionaryValues = dictionary.values
for number in dictionary.keys {
    print("\(number) : \(dictionary[number]!)")
}

//: ### Dictionary Modification
var modifyDictionary = [String: Int]()
modifyDictionary["one"] = 3
modifyDictionary["too"] = 4
modifyDictionary["one"] = 1
modifyDictionary["two"] = 2
modifyDictionary.removeValueForKey("too")
modifyDictionary.removeValueForKey("four")
modifyDictionary.count
modifyDictionary

//: ### Optionals and Dictionaries
let one = dictionary["one"]
let four = dictionary["four"]
if let validNumber = four {
    print("Number has value \(validNumber)")
} else {
    print("Number is nil")
}
func canUnwrap(potentialNumber: Int?) -> Bool {
    if let number = potentialNumber {
        print("Number has value: \(number)")
        return true
    } else {
        return false
    }
}
canUnwrap(one)
canUnwrap(four)
/// Guarded version of unwrapping
func guardedCanUnWrap(potentialNumber: Int?) -> Bool {
    guard let number = potentialNumber else {
        return false
    }
    print("Guarded number has value: \(number)")
    return true
}
guardedCanUnWrap(one)
guardedCanUnWrap(four)

//: ### Enumerate Dictionaries
/// key enumeration
for key in dictionary.keys {
    print("The value of \(key) is \(dictionary[key]!)")
}
/// fast enumeration w/o key -- this is the preferred method
for keyValue in dictionary {
    print("The value of \(keyValue.0) is \(keyValue.1)")
}

//: ### Sets
var odds = Set<Int>()
var primes: Set = [2, 3, 5, 7]

for i in 1.stride(through: 10, by: 2) {
    odds.insert(i)
}
odds.insert(7) // 7 already exists, nothing is "added"
//odds.remove(7)
odds.contains(7)
//odds.intersectInPlace(primes)
odds.count
odds

let union = primes.union(odds)
let intersection = primes.intersect(odds)
let difference = odds.subtract(primes)
let symmetricDifference = odds.exclusiveOr(primes)

intersection.isDisjointWith(union)
intersection.isSubsetOf(union)
intersection.isStrictSubsetOf(union)

for prime in primes {
    print("\(prime)")
}
