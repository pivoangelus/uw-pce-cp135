//: ### Chapter 2 - Functions
//: [TOC](TOC) | [Next](@next)
//: ### Parameters, Overloading, Default Values, and External Names
func hello(name: String = "World", numberOfTimes: Int = 1) {
    for _ in 1 ... numberOfTimes {
        // print("Hello, " + name + "!")
        print("Hello, \(name)!")
    }
}

hello()
hello("Swift Programmer")
hello("Swift 2 Adopter", numberOfTimes: 3)
hello(numberOfTimes: 2)

func hello(personWithName name: String, _ affiliation: String) {
    print("Hello \(name) from \(affiliation).")
}

//hello("Swift Programmer", affiliation: "Dim Sum Thinking")
hello(personWithName: "Swift Programmer", "Dim Sum Thinking")

//: ### Variadic Parameters
func variadicHello(peopleNamed people: String ...) {
    if people.isEmpty {
        variadicHello(peopleNamed: "World")
    }
    for person in people {
        print("Hello, \(person)")
    }
}

variadicHello(peopleNamed: "Swift Programmer", "Swift 2 Developer")
variadicHello()

//: ### print()
let name = "Swift Programmer"
print("Hello, World!")
print("Hello, \(name)!")
// default separator and terminator
print("Hello,", name, "!")
// modified separator
print("Hello,", name, "!", separator: "^_^")
// modified separator and terminator
print("Hello, ", name, "!", separator: "", terminator: "?")

//: ### Return Values
func returnHello(name: String) -> String {
    return "Hello, \(name)!"
}

returnHello("my friend")

//: ### Returning Tuples
func tuplesHello(names: String ...) -> (count: Int, peopleList: String) {
    return (names.count, names.reduce("Hello,"){$0 + "\n " + $1})
}

tuplesHello("Swift Programmer", "Swift 2 Developer")
tuplesHello("Swift Programmer", "Swift 2 Developer").0
tuplesHello("Swift Programmer", "Swift 2 Developer").1
tuplesHello("Swift Programmer", "Swift 2 Developer").count
tuplesHello("Swift Programmer", "Swift 2 Developer").peopleList
