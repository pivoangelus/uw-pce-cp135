//
//  Block.swift
//  Alphabet Block
//
//  Created by Justin Andros on 11/30/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SceneKit

class Block: SCNScene {
    
    override init() {
        super.init()
        
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(0, 0, 5)
        rootNode.addChildNode(cameraNode)
        
        let letters = [
            UIImage(named: "B cap"),
            UIImage(named: "e"),
            UIImage(named: "X cap"),
            UIImage(named: "c"),
            UIImage(named: "x"),
            UIImage(named: "z")
        ]
        
        let lettersNormal = [
            UIImage(named: "B cap normal"),
            UIImage(named: "e normal"),
            UIImage(named: "X cap normal"),
            UIImage(named: "c normal"),
            UIImage(named: "x normal"),
            UIImage(named: "z normal")
        ]
        
        let colors = [
            UIColor.blueColor(),
            UIColor.blueColor(),
            UIColor.yellowColor(),
            UIColor.greenColor(),
            UIColor.orangeColor(),
            UIColor.redColor()
        ]
        
        let tanColor = UIColor(colorLiteralRed: 0.95, green: 0.85, blue: 0.75, alpha: 1)
        
        let box = SCNBox(width: 1, height: 1, length: 1, chamferRadius: 0)
        let boxNode = SCNNode(geometry: box)
        var boxMaterial: [SCNMaterial] = []
        for i in 0..<letters.count {
            let material = SCNMaterial()
            var newImage = letters[i]!.imageWithRenderingMode(.AlwaysTemplate)
            UIGraphicsBeginImageContextWithOptions((letters[i]?.size)!, false, newImage.scale)
            if i == 0 || i == 2 {
                CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), colors[i].CGColor)
                CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, letters[i]!.size.width, letters[i]!.size.height))
                UIColor.whiteColor().set()
                newImage.drawInRect(CGRectMake(0, 0, letters[i]!.size.width, letters[i]!.size.height))
            } else {
                CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), tanColor.CGColor)
                CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, letters[i]!.size.width, letters[i]!.size.height))
                colors[i].set()
                newImage.drawInRect(CGRectMake(0, 0, letters[i]!.size.width, letters[i]!.size.height))
            }
            newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            material.diffuse.contents = newImage
            material.normal.contents = lettersNormal[i]
            boxMaterial.append(material)
        }
        box.materials = boxMaterial
        rootNode.addChildNode(boxNode)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
