//
//  ViewController.swift
//  Alphabet Block
//
//  Created by Justin Andros on 11/30/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {
    
    @IBOutlet weak var scnView_block: SCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scnView_block.scene = Block()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

