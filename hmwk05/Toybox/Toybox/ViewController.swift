//
//  ViewController.swift
//  Toybox
//
//  Created by Justin Andros on 11/30/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import UIKit
import SceneKit
import SpriteKit

class ViewController: UIViewController {

    @IBOutlet weak var scnView_top: SCNView!
    @IBOutlet weak var scnView_bottom: SCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = Toybox()

        scnView_top.scene = scene
        scnView_bottom.scene = scene
        
        let cameraTop = SCNNode()
        cameraTop.camera = SCNCamera()
        cameraTop.position = SCNVector3(0, 0, 25)
        
        let cameraBottom = SCNNode()
        cameraBottom.camera = SCNCamera()
        cameraBottom.position = SCNVector3(0, 20, 15)
        cameraBottom.rotation = SCNVector4(-0.25, 0, 0, 1)
        
        scnView_top.pointOfView = cameraTop
        scnView_bottom.pointOfView = cameraBottom
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func scnView_topTap(sender: UITapGestureRecognizer) {
        viewTapped(sender, scnView: scnView_top)
    }
    
    @IBAction func scnView_bottomTap(sender: UITapGestureRecognizer) {
        viewTapped(sender, scnView: scnView_bottom)
    }

    func viewTapped(gestureRecognize: UITapGestureRecognizer, scnView: SCNView) {
        let moveUp = SCNAction.moveByX(0, y: 2, z: 0, duration: 1)
        let moveDown = SCNAction.moveByX(0, y: -2, z: 0, duration: 1)
        
        let p = gestureRecognize.locationInView(scnView)
        let hitResults = scnView.hitTest(p, options: nil)
        
        for i in 0..<hitResults.count {
            if !hitResults[i].node.geometry!.isKindOfClass(SCNFloor) {
                guard let geometryName = hitResults[i].node.geometry!.name else { return }
                var textNode = SCNNode()
                textNode = createText(name: geometryName, location: hitResults[i].node.position)

                let addText = SCNAction.runBlock({ (_) -> Void in
                    self.scnView_top.scene?.rootNode.addChildNode(textNode)
                    self.scnView_bottom.scene?.rootNode.addChildNode(textNode)
                })
                let removeText = SCNAction.runBlock({ (_) -> Void in
                    textNode.removeFromParentNode()
                })
                
                hitResults[i].node.runAction(SCNAction.sequence([addText, moveUp, moveDown, removeText]))
            }
        }
    }
    
    func createText(name name: String, location: SCNVector3) -> SCNNode {
        let text = SCNText(string: name, extrusionDepth: 0.5)
        text.font = UIFont(name: "ArialMT", size: 2)
        let node = SCNNode(geometry: text)
        node.position = location
        node.position.x += 1
        node.position.y += 1
        return node
    }
}

