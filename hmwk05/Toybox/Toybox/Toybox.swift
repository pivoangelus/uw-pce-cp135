//
//  Toybox.swift
//  Toybox
//
//  Created by Justin Andros on 11/30/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SceneKit

class Toybox: SCNScene {
    
    override init() {
        super.init()
        
        let box = SCNBox(width: 1, height: 1, length: 1, chamferRadius: 0.1)
        box.name = "SCNBox"
        
        let capsule = SCNCapsule(capRadius: 0.5, height: 2)
        capsule.name = "SCNCapsule"
        
        let cone = SCNCone(topRadius: 0.5, bottomRadius: 1, height: 1.5)
        cone.name = "SCNCone"
        
        let cylinder = SCNCylinder(radius: 1, height: 1.5)
        cylinder.name = "SCNCylinder"
        
        let plane = SCNPlane(width: 1, height: 1.5)
        plane.name =  "SCNPlane"
        
        let pyramid = SCNPyramid(width: 2, height: 1.5, length: 1)
        pyramid.name = "SCNPyramid"
        
        let shape = SCNShape(path: UIBezierPath(rect: CGRectMake(0, 0, 1, 1)), extrusionDepth: 1)
        shape.name = "SCNShape"
        
        let sphere = SCNSphere(radius: 1)
        sphere.name = "SCNSphere"
        
        let text = SCNText(string: "T", extrusionDepth: 0.5)
        text.name = "SCNText"
        text.font = UIFont(name: "ArialMT", size: 2)
        
        let torus = SCNTorus(ringRadius: 1, pipeRadius: 0.2)
        torus.name = "SCNTorus"
        
        let tube = SCNTube(innerRadius: 0.5, outerRadius: 1, height: 1.5)
        tube.name = "SCNTube"
        
        let geometries = [box, capsule, cone, cylinder, plane, pyramid, shape, sphere, text, torus, tube]

        let floor = SCNFloor()
        floor.reflectivity = 0.05
        let floorNode = SCNNode(geometry: floor)
        floorNode.position.y = -2.5
        rootNode.addChildNode(floorNode)
        
        let light = SCNLight()
        light.type = SCNLightTypeSpot
        light.castsShadow = true
        light.spotOuterAngle = 180
        light.attenuationEndDistance = 1000
        let lightNode = SCNNode()
        lightNode.light = light
        lightNode.position = SCNVector3(x: 0, y: 10, z: 20)
        lightNode.rotation = SCNVector4(x: 0, y: 0, z: 1, w: 1)
        rootNode.addChildNode(lightNode)
        
        var angle = 0.0
        let radius = 6.0
        let angleIncr = M_PI * 2.0 / Double(geometries.count)
        
        for i in 0..<geometries.count {
            let hue = CGFloat(i)/CGFloat(geometries.count)
            let color = UIColor(hue: hue, saturation: 1, brightness: 1, alpha: 1)
            let geometry = geometries[i]
            geometry.firstMaterial?.diffuse.contents = color
            let node = SCNNode(geometry: geometry)
            let x = radius * cos(angle)
            let z = radius * sin(angle)
            node.position = SCNVector3(x, 0, z)
            rootNode.addChildNode(node)
            angle += angleIncr
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
