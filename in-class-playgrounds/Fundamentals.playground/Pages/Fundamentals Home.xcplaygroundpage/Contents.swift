let string = "Hello Playground"

print (string)

//func hello () {
//    print ("hello world")
//}

//hello()

func hello (name: String) {
    print ("hello", name)
}

hello("everyone")

func hello (name: String = "World", numberOfTimes: Int = 1) { // don't do this
    for _ in 1 ... numberOfTimes {
        hello(name)
    }
}

hello("new Mariners manager", numberOfTimes:12)

// two methods to print variables. no println anymore.
print("hello \(string)")
print("hello", string)
print("hello", string, separator:"")
print("hello", string, separator:"--")

func hi (name: String = "World") {
    hello (name)
}

hi()
hi("justin")
hello()
hello("frankenstein", numberOfTimes:4)
hello(numberOfTimes:4)

func welcome (people: String ...) {
    for person in people {
        hello (person)
    }
}

welcome("larry","moe","curly")

import Foundation

let cities = ["bellevue","seattle","redmond"]
let things = ["car", "hammer", 3]   // Foundation allows us to use NSObject

//welcome(cities) array of strings is not the same as ...

func whatsUp (name: String) -> String {
    return "Hello \(name)"
}

let captain = "Kirk"

let greeting = whatsUp(captain)

func helloFriends (people: String ...) -> (numberOfFriends: Int, greeting: String) {
    var tempGreeting = "Hello "
    for person in people {
        tempGreeting += person + " "
    }
    return (people.count, tempGreeting)
}


let howdy = helloFriends("Larry","Moe","Curley")

print(howdy)
print(howdy.0)
print(howdy.1)
print(howdy.numberOfFriends)
print(howdy.greeting)

