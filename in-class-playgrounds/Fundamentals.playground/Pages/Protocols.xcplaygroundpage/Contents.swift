let vertex = Vertex(x: 3, y: 4)
let sluddedVertex = vertex.moveByX(20)

extension Vertex: CustomStringConvertible {
    public var description: String {
        return "\(x), \(y)"
    }
}

vertex.description
sluddedVertex.description
