enum Tutorial: String {
    case LLDB = "Debugging LLDB"
    case iOS9
    case Swift
    case CoreData
}

var tutorial = Tutorial.iOS9
tutorial = .LLDB
tutorial.rawValue

if let validTutorial = Tutorial(rawValue: "Swift") {
    print("valid: \(validTutorial)")
} else {
    print("nope")
}

