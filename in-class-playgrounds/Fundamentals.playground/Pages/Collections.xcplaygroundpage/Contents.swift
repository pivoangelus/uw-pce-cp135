var numbers = [0, 1, 2, 3]
let one = numbers[1]
print (one)
numbers[1] = 3
print(numbers[1])

import Foundation

let badNumbers  = [0, 1, 2, "cheese sandwich"]
print(badNumbers[3])
let badOne = (badNumbers[1] as! NSNumber).integerValue
let badTwo = (badNumbers[2] as! NSNumber).integerValue
let badCheese = badNumbers[3] as? NSNumber
let sum = badOne + badTwo

var digits = ["one": 1, "two": 2, "three": 3]   // [String : Int] dictionary
digits["two"]
digits.removeValueForKey("three")
digits["three"]
for key in digits.keys { // might be nil so returned as optionals
    print(digits[key])
}

if let fiveValue = digits["five"] {
    print(fiveValue)
} else {
    print("no value")
}

let twoValue = digits["two"]
print(twoValue)
print(twoValue!)
