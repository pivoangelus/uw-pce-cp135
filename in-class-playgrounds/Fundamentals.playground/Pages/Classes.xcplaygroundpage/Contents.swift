class ConferenceAttendee {
    var name: String
    var hometown: String?
    var isFromSomewhere: Bool {
        get {
            return hometown != nil
        }
        set(newValue) {
            if newValue {
                self.hometown = "Carnation"
            } else {
                self.hometown = nil
            }
        }
    }
    
    init() {
        self.name = "Daniel"
        self.hometown = nil
    }
    
    init(name: String, hometown: String? = nil) {
        self.name = name
        self.hometown = hometown
    }
    
    func description() -> String {
        if let hometown = self.hometown {
            return self.name + ", " + hometown
        }
        return self.name
    }
}

let daniel = ConferenceAttendee()
let hal = ConferenceAttendee(name: "Hal", hometown: "Texas")
let justin = ConferenceAttendee(name: "Justin")
hal.description()
daniel.description()
justin.description()
hal.isFromSomewhere
justin.isFromSomewhere
justin.isFromSomewhere = true
justin.isFromSomewhere

class TutorialAttendeee: ConferenceAttendee {
    let tutorial: String
    
    init(name: String, tutorial: String, hometown: String? = nil) {
        self.tutorial = tutorial
        super.init(name: name, hometown: hometown)
    }
}

let andros = TutorialAttendeee(name: "Andros", tutorial: "Core Data")
andros.description()
andros.hometown
