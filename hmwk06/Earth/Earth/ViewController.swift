//
//  ViewController.swift
//  Earth
//
//  Created by Justin Andros on 12/7/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SceneKit
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var earthView_iOS: SCNView!
    
    var earthScene: EarthScene!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.earthScene = EarthScene(view: earthView_iOS)
        self.earthView_iOS.scene = earthScene
    }
    

    @IBAction func viewTapped(sender: UITapGestureRecognizer) {
        self.earthScene.viewTapped(sender)
    }
    
}

