//
//  EarthScene.swift
//  Earth
//
//  Created by Justin Andros on 12/7/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SceneKit
import CoreLocation

#if os(iOS) || os(tvOS)
    import AVFoundation
    private typealias OSColor = UIColor
    private typealias OSImage = UIImage
    private typealias OSFloat = Float
#elseif os(OSX)
    private typealias OSColor = NSColor
    private typealias OSImage = NSImage
    private typealias OSFloat = CGFloat
#endif

class EarthScene: SCNScene {
    
    private var earthView: SCNView!
    private var earthNode: SCNNode!
    
    // lazy variables
    private lazy var pinNode: SCNNode = self.initPinNode()
    private lazy var geocoder = CLGeocoder()
    
    init(view: SCNView) {
        super.init()
        
        self.earthView = view
        self.earthView.backgroundColor = OSColor(white: 0.05, alpha: 1.0)
        self.createEarth()
        self.createCamera()
        self.createSun()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - SCNNodes
    
    func createCamera() {
        let camera = SCNCamera()
        let cameraNode = SCNNode()
        cameraNode.camera = camera
        cameraNode.position = SCNVector3Make(0.0, 0.0, 8.0)
        self.rootNode.addChildNode(cameraNode)
    }
    
    func createEarth() {
        // earth with textures
        let earth = SCNSphere(radius: 3.0)
        let earthMaterial = SCNMaterial()
        earthMaterial.diffuse.contents  = OSImage(named: "earth_diffuse_4k")
        earthMaterial.specular.contents = OSImage(named: "earth_specular_1k")
        earthMaterial.emission.contents = OSImage(named: "earth_lights_4k")
        earthMaterial.normal.contents   = OSImage(named: "earth_normal_4k")
        earthMaterial.multiply.contents = OSColor(white:0.7, alpha: 1.0)
        earthMaterial.shininess         = 0.05
        earth.firstMaterial             = earthMaterial
        
        // place the planet on its axis
        let planet = SCNNode(geometry: earth)
        let axisNode = SCNNode()
        self.rootNode.addChildNode(axisNode)
        axisNode.addChildNode(planet)
        axisNode.rotation = SCNVector4Make(1.0, 0.0, 0.0, OSFloat(M_PI/6.0))
        
        self.earthNode = planet
        
        // larger sphere around the earth for clouds
        let clouds = SCNSphere(radius: 3.075)
        clouds.segmentCount = 144
        let cloudsMaterial = SCNMaterial()
        cloudsMaterial.diffuse.contents = OSColor.whiteColor()
        cloudsMaterial.locksAmbientWithDiffuse = true
        cloudsMaterial.transparent.contents = OSImage(named: "clouds_transparent_2K")
        cloudsMaterial.transparencyMode = SCNTransparencyMode.RGBZero
        cloudsMaterial.writesToDepthBuffer = false
        clouds.firstMaterial = cloudsMaterial
        let cloudsNode = SCNNode(geometry: clouds)
        earthNode.addChildNode(cloudsNode)
        
        earthNode.rotation = SCNVector4Make(0.0, 1.0, 0.0, 0.0)
        cloudsNode.rotation = SCNVector4Make(0.0, 1.0, 0.0, 0.0)
        
        // rotation animation for the earth
        let rotateEarth = CABasicAnimation(keyPath: "rotation.w")
        rotateEarth.byValue = M_PI * 2.0
        rotateEarth.duration = 50
        rotateEarth.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        rotateEarth.repeatCount = Float.infinity
        earthNode.addAnimation(rotateEarth, forKey: "rotate the earth")
        
        // rotation animation for the clouds
        let rotateClouds = CABasicAnimation(keyPath: "rotation.w")
        rotateClouds.byValue = -M_PI * 2.0
        rotateClouds.duration = 150
        rotateClouds.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        rotateClouds.repeatCount = Float.infinity
        cloudsNode.addAnimation(rotateClouds, forKey: "slowly move the clouds")
    }
    
    func createSun() {
        // make sure the earth was created
        if earthNode != nil {
            let sun = SCNLight()
            sun.type = SCNLightTypeSpot
            sun.castsShadow = true
            sun.shadowRadius = 3.0
            sun.shadowColor = OSColor(white: 0.0, alpha: 0.75)
            sun.zNear = 10
            sun.zFar = 40
            let sunNode = SCNNode()
            sunNode.light = sun
            sunNode.position = SCNVector3Make(-15.0, 0.0, 12.0)
            sunNode.constraints = [SCNLookAtConstraint(target: self.earthNode)]
            self.rootNode.addChildNode(sunNode)
        } else {
            print("earthNode needs to be init()")
        }
    }
    
    func placePin(hitResults: [SCNHitTestResult]) {
        // take the first hit
        let hit = hitResults.first
        
        // don't need to continue if there is no result
        if hit == nil { return }
        
        // find its location using the texture map
        let textureCoordinate = hit!.textureCoordinatesWithMappingChannel(0)
        let location = coordinateFromPoint(textureCoordinate)
        self.geocoder.reverseGeocodeLocation(location) { (placemarks, error) -> Void in
            let place = placemarks?.first
            var placeName = place?.country
            if (placeName == nil) { placeName = place?.ocean }
            if (placeName == nil) { placeName = place?.inlandWater }
            if (placeName == nil) { return } // unable to find any location, bail!
            // tell the user the name of the place roughly found
        #if os(iOS) || os(tvOS)
            let utterance = AVSpeechUtterance(string: placeName!)
            let speechSynthesizer = AVSpeechSynthesizer()
            speechSynthesizer.speakUtterance(utterance)
        #elseif os(OSX)
            let speechSynthesizer = NSSpeechSynthesizer()
            speechSynthesizer.startSpeakingString(placeName!)
        #endif
        }
        
        self.pinNode.position = hit!.localCoordinates
        
        let pinDirection = GLKVector3Make(0.0, 1.0, 0.0)
        let normal = SCNVector3ToGLKVector3(hit!.localNormal)
        
        let rotationAxis = GLKVector3CrossProduct(pinDirection, normal)
        let cosAngle = GLKVector3DotProduct(pinDirection, normal)
        
        let rotation = GLKVector4MakeWithVector3(rotationAxis, acos(cosAngle))
        self.pinNode.rotation = SCNVector4FromGLKVector4(rotation)
        self.earthNode.addChildNode(self.pinNode)
    }
    
    // MARK: - Lazy initializers
    
    func initPinNode() -> SCNNode {
        let node = SCNNode()
        let bodyHeight: CGFloat = 0.3
        let bodyRadius: CGFloat = 0.019
        let headRadius: CGFloat = 0.06
        
        let body = SCNCylinder(radius: bodyRadius, height: bodyHeight)
        let head = SCNSphere(radius: headRadius)
        
        let headMaterial = SCNMaterial()
        headMaterial.diffuse.contents   = OSColor.redColor()
        headMaterial.emission.contents  = OSColor(colorLiteralRed: 0.2, green: 0.0, blue: 0.0, alpha: 1.0)
        headMaterial.specular.contents  = OSColor.whiteColor()

        let bodyMaterial = SCNMaterial()
        bodyMaterial.specular.contents  = OSColor.whiteColor()
        bodyMaterial.emission.contents  = OSColor(colorLiteralRed: 0.1, green: 0.1, blue: 0.1, alpha: 1.0)
        bodyMaterial.shininess = 100
        
        head.firstMaterial = headMaterial
        body.firstMaterial = bodyMaterial
        
        let bodyNode = SCNNode(geometry: body)
        bodyNode.position = SCNVector3Make(0.0, OSFloat(bodyHeight / 2.0), 0.0)
        let headNode = SCNNode(geometry: head)
        headNode.position = SCNVector3Make(0.0, OSFloat(bodyHeight), 0.0)
        node.addChildNode(bodyNode)
        node.addChildNode(headNode)

        return node
    }
    
    // MARK: - Converting to latitude & longitude
    
    func coordinateFromPoint(point: CGPoint) -> CLLocation {
        let u = Double(point.x)
        let v = Double(point.y)
        
        let lat: CLLocationDegrees = (0.5 - v) * 180.0
        let lon: CLLocationDegrees = (u - 0.5) * 360.0
        
        return CLLocation(latitude: lat, longitude: lon)
    }
    
    // MARK: - Actions from ViewController
    // To keep things somewhat simple I sent along [SCNHitTestResult], their first common similiarity
#if os(iOS) || os(tvOS)
    func viewTapped(sender: UITapGestureRecognizer) {
        let eventLocation = sender.locationInView(earthView) // CGPoint
        let hitResults = earthView.hitTest(eventLocation, options: [SCNHitTestRootNodeKey: self.earthNode,
                                                                    SCNHitTestIgnoreChildNodesKey: true])
        self.placePin(hitResults)
    }
#elseif os(OSX)
    func viewClicked(event: NSEvent) {
        let eventLocation = event.locationInWindow  // NSPoint
        let hitResults = earthView.hitTest(eventLocation, options: [SCNHitTestRootNodeKey: self.earthNode,
                                                                    SCNHitTestIgnoreChildNodesKey: true])
        self.placePin(hitResults)
    }
#endif
    
}