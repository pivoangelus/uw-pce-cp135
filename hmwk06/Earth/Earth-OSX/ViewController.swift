//
//  ViewController.swift
//  Earth-OSX
//
//  Created by Justin Andros on 12/8/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import Cocoa
import SceneKit

class ViewController: NSViewController {
    
    @IBOutlet weak var earthView_OSX: SCNView!
    
    var earthScene: EarthScene!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.earthScene = EarthScene(view: earthView_OSX)
        self.earthView_OSX.scene = earthScene
        self.earthView_OSX.frame = NSRect(x: 0, y: 0, width: 500, height: 500)
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    override func mouseUp(theEvent: NSEvent) {
        super.mouseUp(theEvent)
        self.earthScene.viewClicked(theEvent)
    }
    
}

