//
//  AppDelegate.swift
//  Earth-OSX
//
//  Created by Justin Andros on 12/8/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }

}

