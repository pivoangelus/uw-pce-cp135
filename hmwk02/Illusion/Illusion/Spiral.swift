//
//  BoxNodes.swift
//  Illusion
//
//  Created by Justin Andros on 10/26/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SpriteKit

class Spiral: SKNode {
    // Box sprites
    var boxSprite: SKShapeNode!
    // Radius (distance) the boxes will be placed at
    var radius: Double!
    // Box stroke colors
    let boxColor = [SKColor.blackColor(), SKColor.whiteColor()]
    
    // Using the ratio from the JavaScript a box size is determined by the radius at which we can fit all the rings
    var boxSide: Double {
        get {
            return (radius * 16) / 65
        }
        set {
            radius = newValue
        }
    }
    
    init (radius: Double, speed: Double, ring: Int) {
        super.init()
        self.radius = radius
        
        // Angle at which the box is placed
        var sp_angle = CGFloat(2 * M_PI)
        // Create a box based on the radius
        let bx_side = CGSize(width: boxSide, height: boxSide)
        
        // Figures out how many boxes we can place
        let n = 2 * M_PI * (radius * 1 / Double(ring)) / boxSide * M_SQRT1_2
        let k = Int(360 / n) - 2
        // Angle at which we want to rotate around the ring as we place a box
        let angleIncrement = sp_angle/(CGFloat(k) - 1)
        
        for var i = 0; i < k; ++i {
            // Place a new sprite at a location around a ring and give it some properities
            boxSprite = SKShapeNode(rectOfSize: bx_side)
            boxSprite.position = CGPoint(x: (cos(sp_angle) * CGFloat(self.radius) * CGFloat(ring) + CGRectGetMidX(self.frame)),
                                         y: (sin(sp_angle) * CGFloat(self.radius) * CGFloat(ring) + CGRectGetMidY(self.frame)))
            boxSprite.strokeColor = boxColor[i % 2]
            boxSprite.lineWidth = 2
            // Add a rotation to the box. All rotations ran at the same speed, some just took longer.
            // If 2 * M_PI is replaced with sp_angle speeds will gradually increase on the boxes
            let action = SKAction.rotateByAngle(CGFloat(2 * M_PI), duration: speed)
            boxSprite.runAction(SKAction.repeatActionForever(action))
            self.addChild(boxSprite)
            sp_angle -= angleIncrement
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
