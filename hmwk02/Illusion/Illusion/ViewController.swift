//
//  ViewController.swift
//  Illusion
//
//  Created by Justin Andros on 10/26/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import UIKit
import SpriteKit

class ViewController: UIViewController {
    
    @IBOutlet weak var mainView: SKView!
    
    var illusion: SCSScene!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bgColor = SKColor(red: 0.47, green: 0.47, blue: 0.47, alpha: 1.0)
        
        view.backgroundColor = bgColor
        illusion = SCSScene(size: mainView.bounds.size)
        illusion.backgroundColor = bgColor
        mainView.presentScene(illusion)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}