//
//  SCSScene.swift
//  Illusion
//
//  Created by Justin Andros on 10/26/15.
//  Copyright © 2015 Justin Andros. All rights reserved.
//

import SpriteKit

class SCSScene: SKScene {
    var spirals: [Spiral] = []
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has no been implemented")
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        // Radius of the first ring
        let radius = Double(size.width / 15)
        // Number of rings is determined by the index of box speeds available.
        let boxSpeed = [6.0, 8.0, 12.0, 12.0, 8.0, 6.0]
        // Rotation speed and duration for each ring
        let rotation = [[CGFloat(2.0 * M_PI), 14],
                        [CGFloat(1.6 * M_PI), 15],
                        [CGFloat(1.0 * M_PI), 16],
                        [CGFloat(-1.0 * M_PI), 16],
                        [CGFloat(-1.6 * M_PI), 15],
                        [CGFloat(-2.0 * M_PI), 14]]
        
        // center of screen
        let centerX = CGRectGetMidX(self.frame)
        let centerY = CGRectGetMidY(self.frame)
    
        for var i = 0; i < boxSpeed.count; ++i {
            // Create a ring and place it in the center of the screen
            self.spirals.append(Spiral(radius: radius, speed: boxSpeed[i], ring: i + 1))
            self.spirals[i].position = CGPoint(x: centerX, y: centerY)
            
            // Add a rotation to the ring
            let rotateAction = SKAction.rotateByAngle(rotation[i][0], duration: Double(rotation[i][1]))
            let action = SKAction.repeatActionForever(rotateAction)
            
            self.spirals[i].runAction(action)
            self.addChild(spirals[i])
        }
    }
}
